package mahoa;

import java.math.BigInteger;
import java.security.MessageDigest;

/*
* @todo: Mã hóa mật khẩu
*/
public final class MaHoaMatKhau {
	/**
	 * Hàm mã hóa mật khẩu
	 *
	 * @since 19/05/2014 HienDM
	 * @param plainText
	 *            Mật khẩu chưa mã hóa
	 * @see encrypt(“123”);
	 */
	public String maHoa(String matKhau) throws Exception {
		MessageDigest crypt = MessageDigest.getInstance("SHA-1");
		crypt.reset();
		crypt.update(matKhau.getBytes("UTF-8"));
		return new BigInteger(1, crypt.digest()).toString(16);
	}
}
